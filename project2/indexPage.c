/* File: indexPage.c */
/* Author: Bailey Whitehill and Jessica Marks */
/* Date: March 26, 2017 */

/* This program indexes a web page, printing out the counts of words on that page */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_ADDR_LENGTH 500

/* Structure holds a letter, how many times that letter is found in as a child of the 
previous letter, and the children of that letter */
struct pathLetter{
  char letter;
  int count;
  struct pathLetter *children[26];
};



/*Method defintions*/
struct pathLetter* indexPage(const char* url);

int addWordOccurrence(char* word, int wordLength, struct pathLetter *root);

void printTrieContents(struct pathLetter *root, int stackTop,char *buffer);

int freeTrieMemory(struct pathLetter *pNode);

int findNextWord(char *pageText, int numBytes, char *wordFound);

int getText(const char* srcAddr, char* buffer, const int bufSize);

int main(int argc, char** argv){
 
 /* argv[1] will be the URL to index, if argc > 1 */
  if (argc>1)
  {
  
  char *startAddr=malloc(sizeof(char)*(MAX_ADDR_LENGTH+1));
  
  //initialize the array that will store the initial web page contents
  char *buffer=malloc(sizeof(char)*500);
  
  //define the root structure
  struct pathLetter *root;
  
    //Print the given url
    printf("%s\n",argv[1]);
    
    if (startAddr != NULL){
      
      //copy the input argument into a char array
      strncpy(startAddr, argv[1], MAX_ADDR_LENGTH+1);
      startAddr[MAX_ADDR_LENGTH - 1] = '\0';
      
      //parse the url and index the contents into root
      root=indexPage(startAddr);
      
      //if the root is null, return -1. Otherwise, print and free the memory
      if (root==NULL){
        return -1;
      }
      else{
        printTrieContents(root,0,buffer);
        freeTrieMemory(root);
      }
    }
    
    //free the url and buffer memory 
    free(startAddr);
    free(buffer);
  }
  else
  {
    //if the argument counter didn't find arguments, quit now
    return -1;
  }
  
  return 0;
}


/* We changed this to return the data structure*/
struct pathLetter* indexPage(const char* url)
{
  
  //initialize variables to be used
	char *pageText=malloc(sizeof(char)*30000);
	int numBytes=0,success, currentIndex=0, i;

	char* wordFound;
	
	struct pathLetter* root;
	root=malloc(sizeof(struct pathLetter));
	root->letter='0';
  root->count=0;
 
 //set all children to null initially
  for (i=0;i<26;i++){
    root->children[i]=NULL;
  }
	
	//get the number of bytes found on the page
	numBytes=getText(url,pageText,30000);
	
	
		while(numBytes>0)
	{
	  
	  char *wordFound=malloc(sizeof(char)*500);
	  
		//determine the next word from the buffer
		int checkChar=findNextWord(pageText+currentIndex, numBytes, wordFound);
	
  	//check to make sure it successfully found a word
		if (checkChar!=1){
		  //add the word to the trie contents
		  success=addWordOccurrence(wordFound, strlen(wordFound), root);
		  
		  if(success!=0)
		  {
		    return NULL;    
		  }
		  
		  //print the added word
		  printf("\t%s\n", wordFound);
		}
			
		
		
		//how many bytes left in the page to read
		numBytes=numBytes-checkChar;
		
		//where we read from in the text
		currentIndex=currentIndex+checkChar;
    
    //free the memory used to hold the individual word
    free(wordFound);
	}
	
   
   
   
  //at this point we have reached the end of the web page array and have a complete trie with proper counts
  free(pageText);
 
  return root;
  
}


//this method finds the next word from the page text char array
int findNextWord(char *pageText, int totalLength, char *wordFound)
{
	
	int startLength=totalLength;
	int stopWord=1;
	int checkChar=0;
	char currentChar;
	int index=0;
	
	
	while(startLength>checkChar && stopWord==1)
	{
	 //get the current char
	 currentChar=pageText[checkChar];
		
			//add the letter to the word buffer if it's lowercase
		  if ((currentChar>='a') && (currentChar<='z'))
		  {
			    wordFound[index]=currentChar;
			   
			    //now set to check the next char and increase the index
			    checkChar++;
			    index++;
			  
		  }
		  
		  //Check if the Character is an uppercase letter
		  else if (currentChar>='A' && currentChar<='Z'){
		    
		    //change it to lower then use it
		    wordFound[checkChar]=(char) tolower(currentChar);
		    
			 //now check next index
			 checkChar++;
			 index++;
		  
	  	}
		  else
		  {
		  	//the char found is a separator or non-letter, we have found a word
			  checkChar++;
			  stopWord=0;
			  wordFound[index]='\0';
		  }
	  }
	
	return checkChar;
}

//this method adds the word found to the trie structure
int addWordOccurrence(char* word, int wordLength, struct pathLetter *root)
{
  
  //Assumes that empty children are initialized to null in the root
  int i;
  int firstEmptySpace;
  
  //Increment Counter if the entire word is added
  if (wordLength==0){
    root->count++;
    return 0;
  }
  
  //Loop through children to check if character is added
  for (i=0;i<26;i++){
    if (root->children[i]!=NULL){
      if ((root->children[i]->letter)==*word){
        //If already added, move to next
        return addWordOccurrence(word+1,wordLength-1,root->children[i]);
      }
    }
  }
  
  //Find first empty space in children
  for (i=0; i<26;i++){
    if (root->children[i]==NULL){
      firstEmptySpace=i;
      break;
    }
  }
  
  //Move children around to insert new node into correct alphabetical slot
  while((firstEmptySpace!=0) && ((root->children[firstEmptySpace-1]->letter)>*word)){
    
    //Shift elements right while element before empty space is greater than character we are inserting
    //Such that if c is inserted into a node with children (a,d), d is shifted right and c is placed between them
    root->children[firstEmptySpace]=root->children[firstEmptySpace-1];
    firstEmptySpace--;
  }
  
  //Create new node
  struct pathLetter *newNode;
  newNode=malloc(sizeof(struct pathLetter));
  if (newNode==NULL){
    return -1;
  }
  newNode->letter=*word;
  newNode->count=0;
  for (i=0;i<26;i++){
    newNode->children[i]=NULL;
  }
  
  
  root->children[firstEmptySpace]=newNode;
  
  return addWordOccurrence(word+1,wordLength-1,newNode);
}


void printTrieContents(struct pathLetter *root, int stackTop,char *buffer)
{
  
  //Pass zero as stacktop for first iteration
  char tempString[30000];
  int i;
  
  //if the letter is not empty, then place it in the buffer and increase current buffer index
  if (root->letter!='0'){
    buffer[stackTop]=root->letter;
    stackTop++;
    
    //if the count of the letter is not 0 then put null in the buffer and print
    if (root->count!=0){
      buffer[stackTop]='\0';
      strncpy(tempString,buffer,30000);
      
      //show the word and the count
      printf("%s: %d\n",tempString,root->count);
    }
  }
  
  //go through the trie and print the above for each node
  for (i=0;i<26;i++){
    if (root->children[i]!=NULL){
      printTrieContents(root->children[i],stackTop,buffer);
    }
  }
  
}

int freeTrieMemory(struct pathLetter *pNode)
{
  

  int i;
  
  //go through each node and free the memory
  for(i=0; i<26;i++){
   if (pNode == NULL)
    {
      return;
    }
    else
    {
      if (pNode->children[i]!=NULL){
        freeTrieMemory(pNode->children[i]);
      }
    }
  }
  free(pNode);
}

/* You should not need to modify this function */
int getText(const char* srcAddr, char* buffer, const int bufSize){
  FILE *pipe;
  int bytesRead;

  snprintf(buffer, bufSize, "curl -s \"%s\" | python getText.py", srcAddr);

  pipe = popen(buffer, "r");
  if(pipe == NULL){
    fprintf(stderr, "ERROR: could not open the pipe for command %s\n",
	    buffer);
    return 0;
  }

  bytesRead = fread(buffer, sizeof(char), bufSize-1, pipe);
  buffer[bytesRead] = '\0';

  pclose(pipe);

  return bytesRead;
}