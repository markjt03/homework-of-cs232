#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "trie_list.h"
#define MAX_BUFFER 100
#define MAX_WORD 50
#define MAX_NUM_WORDS 10

void uiStart(struct trieListNode *head){
    char * buffer=malloc(sizeof(char)*MAX_BUFFER);
    if (buffer==NULL){
        return;
    }
    char * token=NULL;
    const int max_addr_length=1000;
    char **words=NULL;
    char **urls=NULL;
    double *scores=NULL;
    char buffer2[MAX_BUFFER];
    char buffer3[MAX_BUFFER];
    int numWords;
    int length=0;
    int i=0;
    int j=1;
    
    do{
        printf("\nEnter a web query: ");
        if(fgets(buffer,MAX_BUFFER,stdin)==NULL){
            printf("Exiting the program\n");
            break;
        }
        else{
            if (buffer[0]=='\n'){
                printf("Exiting the program\n");
                break;
            }
            for (i=0;i<strlen(buffer);i++){
                if (buffer[i]=='\n'){
                    buffer[i]='\0';
                }
            }
            strncpy(buffer2,buffer,MAX_BUFFER);
            strncpy(buffer3,buffer,MAX_BUFFER);
            if(checkValidity(buffer2)!=0){
                printf("Please enter a query containing only lower-case letters.\n");
                continue;
            }
            else{
                words=malloc(sizeof(char*)*MAX_NUM_WORDS);
                if (words==NULL){
                    return;
                }
                for (i=0;i<MAX_NUM_WORDS;i++){
                    words[i]=malloc(sizeof(char)*MAX_WORD);
                    if (words[i]==NULL){
                        return;
                    }
                }
                int i=0;
                token=strtok(buffer3," ");
                while(token!=NULL){
                    strncpy(words[i],token,MAX_WORD);
                    token=strtok(NULL," ");
                    i++;
                }
            
            
                numWords=i;
                urls=malloc(sizeof(char*)*3);
                if (urls==NULL){
                    return;
                }
                for (i=0;i<3;i++){
                    urls[i]=malloc(sizeof(char)*max_addr_length);
                    if (urls[i]==NULL){
                        return;
                    }
                }
                scores=malloc(sizeof(double)*3);
                if (scores==NULL){
                    return;
                }
                listLength(head,&length);
                printf("Query is \"%s\".\n",buffer);
                printf("IDF scores are\n");
                getBestScores(words,urls,scores,length,head,numWords);
                for (i=0;i<numWords;i++){
                    printf("IDF(%s): %.7f\n",words[i],idf(words[i],head));
                }
                j=1;
                printf("Web pages:\n");
                for (i=0;i<3;i++){
                    if (scores[i]!=0){
                    printf("%d. %s (score: %.4f)\n",j,urls[i],scores[i]);
                    j++;
                    }
                }
                for (i=0;i<MAX_NUM_WORDS;i++){
                    free(words[i]);
                }
                free(words);
                for (i=0;i<3;i++){
                    free(urls[i]);
                }
                free(urls);
                free(scores);
            
                continue;
            }
        }
    }
    while(1); 
    free(buffer);
}
int checkValidity(char *buffer,char *words){
    char * token;
    token=strtok(buffer," ");
    int j=0;
    while(token!=NULL){  
        if (strncmp(token," ",MAX_WORD)==0){
            token=strtok(NULL," ");
            continue;
        }
        for (j=0;j<strlen(token);j++){
            if (islower(token[j])==0){
                return -1;
             }
            
        }
        token=strtok(NULL," ");
    }
    return 0;
}
