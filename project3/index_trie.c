#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "index_trie.h"
#define MAX_BUFFER 200000
struct pathLetter* indexPage(const char* url, int *totalTerms)
{
  
  //initialize variables to be used
	char *pageText=malloc(sizeof(char)*MAX_BUFFER);
	int numBytes=0,success, currentIndex=0, i;
  *totalTerms=0;
	char* wordFound;
	
	struct pathLetter* root;
	root=malloc(sizeof(struct pathLetter));
	root->letter='0';
  root->count=0;
  printf("%s\n",url);
 //set all children to null initially
  for (i=0;i<26;i++){
    root->children[i]=NULL;
  }
	
	//get the number of bytes found on the page
	numBytes=getText(url,pageText,MAX_BUFFER);
	
	
		while(numBytes>0)
	{
	  
	  char *wordFound=malloc(sizeof(char)*500);
	  
		//determine the next word from the buffer
		int checkChar=findNextWord(pageText+currentIndex, numBytes, wordFound);
	
  	//check to make sure it successfully found a word
		if (checkChar!=1){
		  //add the word to the trie contents
		  success=addWordOccurrence(wordFound, strlen(wordFound), root);
		  
		  if(success!=0)
		  {
		    return NULL;    
		  }
		  (*totalTerms)++;
		  //print the added word
		  printf("\t%s\n", wordFound);
		}
			
		
		
		//how many bytes left in the page to read
		numBytes=numBytes-checkChar;
		
		//where we read from in the text
		currentIndex=currentIndex+checkChar;
    
    //free the memory used to hold the individual word
    free(wordFound);
	}
	//at this point we have reached the end of the web page array and have a complete trie with proper counts
  free(pageText);
  return root;
  
}

int findNextWord(char *pageText, int totalLength, char *wordFound)
{
	
	int startLength=totalLength;
	int stopWord=1;
	int checkChar=0;
	char currentChar;
	int index=0;
	
	
	while(startLength>checkChar && stopWord==1)
	{
	 //get the current char
	 currentChar=pageText[checkChar];
		
			//add the letter to the word buffer if it's lowercase
		  if ((currentChar>='a') && (currentChar<='z'))
		  {
			    wordFound[index]=currentChar;
			   
			    //now set to check the next char and increase the index
			    checkChar++;
			    index++;
			  
		  }
		  
		  //Check if the Character is an uppercase letter
		  else if (currentChar>='A' && currentChar<='Z'){
		    
		    //change it to lower then use it
		    wordFound[checkChar]=(char) tolower(currentChar);
		    
			 //now check next index
			 checkChar++;
			 index++;
		  
	  	}
		  else
		  {
		  	//the char found is a separator or non-letter, we have found a word
			  checkChar++;
			  stopWord=0;
			  wordFound[index]='\0';
		  }
	  }
	
	return checkChar;
}

int addWordOccurrence(char* word, int wordLength, struct pathLetter *root)
{
  
  //Assumes that empty children are initialized to null in the root
  int i;
  int firstEmptySpace;
  
  //Increment Counter if the entire word is added
  if (wordLength==0){
    root->count++;
    return 0;
  }
  
  //Loop through children to check if character is added
  for (i=0;i<26;i++){
    if (root->children[i]!=NULL){
      if ((root->children[i]->letter)==*word){
        //If already added, move to next
        return addWordOccurrence(word+1,wordLength-1,root->children[i]);
      }
    }
  }
  
  //Find first empty space in children
  for (i=0; i<26;i++){
    if (root->children[i]==NULL){
      firstEmptySpace=i;
      break;
    }
  }
  
  //Move children around to insert new node into correct alphabetical slot
  while((firstEmptySpace!=0) && ((root->children[firstEmptySpace-1]->letter)>*word)){
    
    //Shift elements right while element before empty space is greater than character we are inserting
    //Such that if c is inserted into a node with children (a,d), d is shifted right and c is placed between them
    root->children[firstEmptySpace]=root->children[firstEmptySpace-1];
    firstEmptySpace--;
  }
  
  //Create new node
  struct pathLetter *newNode;
  newNode=malloc(sizeof(struct pathLetter));
  if (newNode==NULL){
    return -1;
  }
  newNode->letter=*word;
  newNode->count=0;
  for (i=0;i<26;i++){
    newNode->children[i]=NULL;
  }
  
  
  root->children[firstEmptySpace]=newNode;
  
  return addWordOccurrence(word+1,wordLength-1,newNode);
}

void printTrieContents(struct pathLetter *root, int stackTop,char *buffer)
{
  
  //Pass zero as stacktop for first iteration
  char tempString[MAX_BUFFER];
  int i;
  
  //if the letter is not empty, then place it in the buffer and increase current buffer index
  if (root->letter!='0'){
    buffer[stackTop]=root->letter;
    stackTop++;
    
    //if the count of the letter is not 0 then put null in the buffer and print
    if (root->count!=0){
      buffer[stackTop]='\0';
      strncpy(tempString,buffer,MAX_BUFFER);
      
      //show the word and the count
      printf("%s: %d\n",tempString,root->count);
    }
  }
  
  //go through the trie and print the above for each node
  for (i=0;i<26;i++){
    if (root->children[i]!=NULL){
      printTrieContents(root->children[i],stackTop,buffer);
    }
  }
  
}

int freeTrieMemory(struct pathLetter *pNode)
{
  int i;
  //go through each node and free the memory
  for(i=0; i<26;i++){
   if (pNode == NULL)
    {
      return;
    }
    else
    {
      if (pNode->children[i]!=NULL){
        freeTrieMemory(pNode->children[i]);
      }
    }
  }
  free(pNode);
}

/* You should not need to modify this function */
int getText(const char* srcAddr, char* buffer, const int bufSize){
  FILE *pipe;
  int bytesRead;

  snprintf(buffer, bufSize, "curl -s \"%s\" | python getText.py", srcAddr);

  pipe = popen(buffer, "r");
  if(pipe == NULL){
    fprintf(stderr, "ERROR: could not open the pipe for command %s\n",
	    buffer);
    return 0;
  }

  bytesRead = fread(buffer, sizeof(char), bufSize-1, pipe);
  buffer[bytesRead] = '\0';

  pclose(pipe);

  return bytesRead;
}

int getCount(const char *word, struct pathLetter*root,const int wordLength){
  int i=0;
  if (wordLength==0){
    return root->count;
  }
  for (i=0;i<26;i++){
    if (root->children[i]!=NULL){
      if ((root->children[i]->letter)==*word){
        return getCount(word+1,root->children[i],wordLength-1);
      }
    }
  }
  return 0;
  
}