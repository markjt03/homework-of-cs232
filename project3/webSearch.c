#include "linked_list_crawler.h"
#include "trie_list.h"
#include "index_trie.h"
#include "ui.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(int argc, char** argv){
    if(argc != 4){
        fprintf(stderr, "USAGE: %s fileName maxPages rand seed", argv[0]);
        return -1;
    }
    const int maxFileSize=100;
    char filename[maxFileSize];
    strncpy(filename,argv[1],maxFileSize);
    const int maxPages=atoi(argv[2]);
    long seed=atol(argv[3]);
    char buffer[MAX_ADDR_LENGTH];
    char destAddr[MAX_ADDR_LENGTH];
    char addr[MAX_ADDR_LENGTH];
    int numHops;
    int n=0;
    int res;
    int hopNum;
    struct listNode *pListStart;
    struct pathLetter *trie;
    struct trieListNode *trieList;
    int totalTerms;
    int firstAddr=1;
    
    FILE *fp=NULL;
    fp=fopen(filename,"r");
    if (fp==NULL){
        fprintf(stderr, "Could not open the file (%s).\n",filename);
        return -1;
    }
    pListStart = malloc(sizeof(struct listNode));
    pListStart->next = NULL;
    if(pListStart == NULL){
        fprintf(stderr, "ERROR: could not allocate memory\n");
        return -2;
        }
    //This is to avoid conditional jump error in contains, initializing first addr to 0
    strncpy(pListStart->addr,"0",MAX_ADDR_LENGTH);
    trieList=malloc(sizeof(struct trieListNode));
    if (trieList==NULL){
        fprintf(stderr, "ERROR: could not allocate memory\n");
        return -1;
    }
    trieList->url=malloc(sizeof(char*)*MAX_ADDR_LENGTH);
    if (trieList->url==NULL){
        return -1;
        fprintf(stderr, "ERROR: could not allocate memory\n");
    }
    srand(seed);
    //This is also initialized to zero to get around condidtional jump error
    strncpy(trieList->url,"0",MAX_ADDR_LENGTH);
    trieList->next=NULL;
    while (fgets(buffer,MAX_ADDR_LENGTH,fp)!=NULL && n<maxPages){
        if (n==0){
            printf("%s\n","Indexing...");
        }
        res=sscanf(buffer,"%s%d",addr,&numHops);
        if (res!=2){
            fprintf(stderr,"Invalid file format (%s).\n",filename);
            break;
        }
        
        hopNum=0;
        
        while(1){
            
            if (contains(pListStart,addr)==0){
                insertBack(pListStart,addr);
                trie=indexPage(addr,&totalTerms);
                //printf("%d\n",totalTerms);
                //Save address, it's associated trie and it's total word count into trieList
                insert(addr,trieList,trie,totalTerms);
                n++;
            }
            
            hopNum++;
            
            if (hopNum<=numHops && n<maxPages){
                
                res=getLink(addr,destAddr,MAX_ADDR_LENGTH);
                if (!res){
                    break;
                }
                strncpy(addr,destAddr,MAX_ADDR_LENGTH);
            }
            else{
                break;
            }
        }
        
    }
    uiStart(trieList);
    destroyList(pListStart);
    destroyTrieList(trieList);
    fclose(fp);
}
    