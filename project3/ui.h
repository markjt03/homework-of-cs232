#ifndef ui_h
#define ui_h

void uiStart(struct trieListNode *head);
int checkValidity(char *buffer);

#endif
