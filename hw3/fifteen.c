/**
 * fifteen.c
 *
 * Computer Science 50
 * Problem Set 3
 *
 * Implements Game of Fifteen (generalized to d x d).
 *
 * Usage: fifteen d
 *
 * whereby the board's dimensions are to be d x d,
 * where d must be in [DIM_MIN,DIM_MAX]
 *
 * Note that usleep is obsolete, but it offers more granularity than
 * sleep and is simpler to use than nanosleep; `man usleep` for more.
 */
 
#define _XOPEN_SOURCE 500

#include <cs50.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// constants
#define DIM_MIN 3
#define DIM_MAX 9

// board
int board[DIM_MAX][DIM_MAX];

// dimensions
int d;

// prototypes
void clear(void);
void greet(void);
void init(void);
void draw(void);
bool move(int tile);
bool won(void);

int main(int argc, string argv[])
{
    // ensure proper usage
    if (argc != 2)
    {
        printf("Usage: fifteen d\n");
        return 1;
    }

    // ensure valid dimensions
    d = atoi(argv[1]);
    if (d < DIM_MIN || d > DIM_MAX)
    {
        printf("Board must be between %i x %i and %i x %i, inclusive.\n",
            DIM_MIN, DIM_MIN, DIM_MAX, DIM_MAX);
        return 2;
    }

    // open log
    FILE* file = fopen("log.txt", "w");
    if (file == NULL)
    {
        return 3;
    }

    // greet user with instructions
    greet();

    // initialize the board
    init();

    // accept moves until game is won
    while (true)
    {
        // clear the screen
        clear();

        // draw the current state of the board
        draw();

        // log the current state of the board (for testing)
        for (int i = 0; i < d; i++)
        {
            for (int j = 0; j < d; j++)
            {
                fprintf(file, "%i", board[i][j]);
                if (j < d - 1)
                {
                    fprintf(file, "|");
                }
            }
            fprintf(file, "\n");
        }
        fflush(file);

        // check for win
        if (won())
        {
            printf("ftw!\n");
            break;
        }

        // prompt for move
        printf("Tile to move: ");
        int tile = GetInt();
        
        // quit if user inputs 0 (for testing)
        if (tile == 0)
        {
            break;
        }

        // log move (for testing)
        fprintf(file, "%i\n", tile);
        fflush(file);

        // move if possible, else report illegality
        if (!move(tile))
        {
            printf("\nIllegal move.\n");
            usleep(500000);
        }

        // sleep thread for animation's sake
        usleep(500000);
    }
    
    // close log
    fclose(file);

    // success
    return 0;
}

/**
 * Clears screen using ANSI escape sequences.
 */
void clear(void)
{
    printf("\033[2J");
    printf("\033[%d;%dH", 0, 0);
}

/**
 * Greets player.
 */
void greet(void)
{
    clear();
    printf("WELCOME TO GAME OF FIFTEEN\n");
    usleep(2000000);
}

/**
 * Initializes the game's board with tiles numbered 1 through d*d - 1
 * (i.e., fills 2D array with values but does not actually print them).  
 */
void init(void)
{
    int i;
    int j;
    int maxNumber=(d*d)-1;
    int tempTile;
    int count=0;
    for(i=0;i<d;i++){
        for (j=0;j<d;j++){
            /*Put a negative one to mark the 'open space' in the last position */
            if((i==d-1) && (j==d-1)){
                board[i][j]=-1;
            }
            /*Otherwise, fill the board with numbers descending from d*d-1 to 1 */
            else{
                board[i][j]=maxNumber-count;
                count++;
            }
        }
    }
    /*Swap last two tiles if odd total number of tiles*/
    if (maxNumber%2==1){
        tempTile=board[d-1][d-2];
        board[d-1][d-2]=board[d-1][d-3];
        board[d-1][d-3]=tempTile;
    }
}

/**
 * Prints the board in its current state.
 */
void draw(void)
{
    /*Print the integers on the board, and an underscore in the spot that has -1*/
    int i;
    int j;
    for (i=0;i<d;i++){
        for (j=0;j<d;j++){
            if (board[i][j]!=-1)
                printf("%d\t",board[i][j]);
            else
                printf("_\t");
        }
        printf("\n");
    }
}

/**
 * If tile borders empty space, moves tile and returns true, else
 * returns false. 
 */
bool move(int tile)
{
    // TODO
    int i;
    int j;
    int rowIndex;
    int columnIndex;
    int tileTemp=tile;
    /*Find index of tile*/
    for (i=0;i<d;i++){
        for (j=0;j<d;j++){
            if(board[i][j]==tile){
                rowIndex=i;
                columnIndex=j;
                break;
            }
        }
    }
    /*Check Neighbors for the empty tile, could implement as function instead, but would require working outside of todo's*/
    if (board[rowIndex+1][columnIndex]==-1){
        board[rowIndex][columnIndex]=board[rowIndex+1][columnIndex];
        board[rowIndex+1][columnIndex]=tileTemp;
        return true;
    }
    else if(board[rowIndex-1][columnIndex]==-1){
        board[rowIndex][columnIndex]=board[rowIndex-1][columnIndex];
        board[rowIndex-1][columnIndex]=tileTemp;
        return true;
    }
    else if(board[rowIndex][columnIndex+1]==-1){
        board[rowIndex][columnIndex]=board[rowIndex][columnIndex+1];
        board[rowIndex][columnIndex+1]=tileTemp;
        return true;
    }
    else if(board[rowIndex][columnIndex-1]==-1){
        board[rowIndex][columnIndex]=board[rowIndex][columnIndex-1];
        board[rowIndex][columnIndex-1]=tileTemp;
        return true;
    }
    else
        return false;
}

/**
 * Returns true if game is won (i.e., board is in winning configuration), 
 * else false.
 */
bool won(void)
{
    // TODO
    int i;
    int j;
    int count=1;
    int boardCorrect=0;
    /*Check if board contains numbers in sequence, and last square empty*/
    for (i=0;i<d;i++){
        for(j=0;j<d;j++){
            /*If on last square, check for -1*/
            if ((i==d-1) && (j==d-1)){
                if (board[i][j]==-1){
                    boardCorrect=1;
                }
            }
            /*Else check squares in order*/
            else if(board[i][j]==count){
                    count++;
                    boardCorrect=1;
            }
            else {
                boardCorrect=0;
                break;
                }
        }
        if (boardCorrect==0){
            break;
        }
    }
    if (boardCorrect==1)
        return true;
    else
        return false;
}