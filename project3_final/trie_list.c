#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "trie_list.h"
#include "index_trie.h"


//Return score for an individual node
double getScore(char **words,struct trieListNode *current, struct trieListNode *head,double *score,int numWords){
    int i=0;
    *score=0;
    for (i=0;i<numWords;i++){
        (*score)+=(1.0*getCount(words[i],current->trieRoot,strlen(words[i]))/((current->totalTerms)))*idf(words[i],head);
    }
    return *score;
}
//Return top scores, and urls--
int getBestScores(char **words,char **url, double *scoreArray,int totalDocuments,struct trieListNode *head,int numWords){
    char **allUrls=malloc(sizeof(char*)*totalDocuments);
    if (allUrls==NULL){
        return -1;
    }
    int success;
    int i;
    int j;
    double previousBest=0;
    if(allUrls==NULL){
        return -1;
    }
    for (i=0;i<totalDocuments;i++){
        allUrls[i]=malloc(sizeof(char)*MAX_ADDR_LENGTH);
        if (allUrls[i]==NULL){
            return -1;
        }
    }
    double *allScores=malloc(sizeof(double)*totalDocuments);
    for (i=0;i<totalDocuments;i++){
        allScores[i]=0;
    }
    double bestScore=0;
    int bestIndex=0;
    success=getAllScores(words,allUrls,allScores,head,numWords);
    if (success!=0){
        return -1;
    }
    else{
        for (j=0;j<3;j++){
            bestScore=0;
            for (i=0;i<totalDocuments;i++){
                    if (allScores[i]>bestScore){
                        bestScore=allScores[i];
                        bestIndex=i;
                    }
                
            }
            scoreArray[j]=bestScore;
            strncpy(url[j],allUrls[bestIndex],MAX_ADDR_LENGTH);
            allScores[bestIndex]=0;
        }
    }
    free(allScores);
    for (i=0;i<totalDocuments;i++){
        free(allUrls[i]);
    }
    free(allUrls);
    return 0;
}
//Gets all scores    
int getAllScores(char **words,char **url, double *scoreArray,struct trieListNode *head,int numWords){
    int totalListLength=0;
    int i=0;
    double score=0;
    struct trieListNode *current=head;
    listLength(head,&totalListLength);
    while(current!=NULL){
        getScore(words,current,head,&score,numWords);
        scoreArray[i]=score;
        strncpy(url[i],current->url,MAX_ADDR_LENGTH);
        i++;
        score=0;
        current=current->next;
    }
    return 0;
}

int insert(char *url,struct trieListNode* head,struct pathLetter *root,int totalTerms){
  //Value of zero for url indicates we're at the start of list, create first node
  if (strncmp(head->url,"0",MAX_ADDR_LENGTH)==0){
      strncpy(head->url,url,MAX_ADDR_LENGTH);
      head->url[MAX_ADDR_LENGTH-1]='\0';
      head->trieRoot=root;
      head->totalTerms=totalTerms;
      head->next=NULL;
      return 0;
  }  
  //Go through list til first empty node
  else if (head->next!=NULL){
      return insert(url,head->next,root,totalTerms);
  }
  //Create new node at next
  else{
    head->next=malloc(sizeof(struct trieListNode));
    if (head->next==NULL){
        return -1;
    } 
    head->next->url=malloc(sizeof (char*)*MAX_ADDR_LENGTH);
    strncpy(head->next->url,url,MAX_ADDR_LENGTH);
    head->next->url[MAX_ADDR_LENGTH-1]='\0';
    head->next->trieRoot=root;
    head->next->totalTerms=totalTerms;
    head->next->next=NULL;
    return 0;
    
  }
}
int destroyTrieList(struct trieListNode *head){
    //Free the trie list, tries, and urls 
    struct trieListNode *next=head->next;
    if (next!=NULL){
        freeTrieMemory(head->trieRoot);
        free(head->url);
        free(head);
        destroyTrieList(next);
    }
    else{
        freeTrieMemory(head->trieRoot);
        free(head->url);
        free(head);
        return 0;
    }
 
}
//TODO intended to count the total number of times a word occurs over all documents
int countWord(struct trieListNode *head, char *word,int *counter){
    if (head!=NULL){
        if (getCount(word,head->trieRoot,strlen(word))!=0){
            (*counter)++;
        }
    }
    if (head->next!=NULL){
        countWord(head->next,word,counter);
    }
}
//Calculate idf
double idf(char * word, struct trieListNode *head){
    int totalDocumentAppearances; 
    int totalListLength;
    totalDocumentAppearances=0;
    countWord(head,word,&totalDocumentAppearances);
    totalListLength=0;
    listLength(head,&totalListLength);
    return log(1.0+(1.0*totalListLength))-log(1.0+(1.0*totalDocumentAppearances));
    
}
int listLength(struct trieListNode *head,int *counter){
    if (head !=NULL){
        (*counter)++;
    }
    if (head->next!=NULL){
        listLength(head->next,counter);
    }
}