#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "linked_list_crawler.h"
int contains(const struct listNode *pNode, const char *addr){
  if (pNode==NULL){
    return 0;
  }
  else if (strncmp(pNode->addr,addr,MAX_ADDR_LENGTH)==0){
    return 1;
  }
  else{
    contains(pNode->next,addr);
  } 
}
void insertBack(struct listNode *pNode, const char *addr){
 /*TODO: complete this*/
  /*struct listNode *current=pNode;*/
  if (pNode->next!=NULL){
      insertBack(pNode->next,addr);
  }
  else{
    pNode->next=malloc(sizeof(struct listNode));
    if (pNode->next!=NULL){
      strncpy(pNode->next->addr,addr,MAX_ADDR_LENGTH);
      pNode->next->addr[MAX_ADDR_LENGTH-1]='\0';
      pNode->next->next=NULL;
    }
  }
}

void printAddresses(const struct listNode *pNode){
 /*TODO: complete this*/
  if (strncmp(pNode->addr,"0",MAX_ADDR_LENGTH)!=0){
    printf("%s\n",pNode->addr);
  }
  if (pNode->next!=NULL){
    printAddresses(pNode->next);
  }
 
}

void destroyList(struct listNode *pNode){
 /*TODO: complete */
 struct listNode *next=pNode->next;
 if (next!=NULL){
   free(pNode);
   destroyList(next);
 }
 else{
   free(pNode);
 }
 
}
int getLink(const char* srcAddr, char* link, const int maxLinkLength){
  const int bufSize = 1000;
  char buffer[bufSize];

  int numLinks = 0;

  FILE *pipe;

  snprintf(buffer, bufSize, "curl -s \"%s\" | python getLinks.py", srcAddr);

  pipe = popen(buffer, "r");
  if(pipe == NULL){
    fprintf(stderr, "ERROR: could not open the pipe for command %s\n",
	    buffer);
    return 0;
  }

  fscanf(pipe, "%d\n", &numLinks);

  if(numLinks > 0){
    int linkNum;
    double r = (double)rand() / ((double)RAND_MAX + 1.0);

    for(linkNum=0; linkNum<numLinks; linkNum++){
      fgets(buffer, bufSize, pipe);
      
      if(r < (linkNum + 1.0) / numLinks){
		break;
      }
    }

    /* copy the address from buffer to link */
    strncpy(link, buffer, maxLinkLength);
    link[maxLinkLength-1] = '\0';
    
    /* get rid of the newline */
    {
      char* pNewline = strchr(link, '\n');
      if(pNewline != NULL){
		*pNewline = '\0';
      }
    }
  }

  pclose(pipe);

  if(numLinks > 0){
    return 1;
  }
  else{
    return 0;
  }
}

