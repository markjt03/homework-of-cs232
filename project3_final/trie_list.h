#ifndef TRIE_LIST_H
#define TRIE_LIST_H

struct trieListNode{
    char *url;
    struct pathLetter* trieRoot;
    int totalTerms;
    struct trieListNode* next;
};


double getScore(char **word,struct trieListNode *current,struct trieListNode *head,double *score,int numWords);
int getBestScores(char **words,char **url, double *scoreArray,int totalDocuments,struct trieListNode *head,int numWords);
int getAllScores(char **words,char **url, double *scoreArray,struct trieListNode *head,int numWords);
int insert(char *url,struct trieListNode* head,struct pathLetter*root,int totalTerms);
int destroyTrieList(struct trieListNode *head);
int countWord(struct trieListNode *head,char *word,int *counter);
double idf(char * word, struct trieListNode *head);
int listLength(struct trieListNode *head,int *counter);

#endif