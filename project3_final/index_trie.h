#ifndef MAX_ADDR_LENGTH
#define MAX_ADDR_LENGTH 1000
#endif
#ifndef INDEX_TRIE_H
#define INDEX_TRIE_H

struct pathLetter{
  char letter;
  int count;
  struct pathLetter *children[26];
};

struct pathLetter* indexPage(const char* url, int * totalTerms);
int addWordOccurrence(char* word, int wordLength, struct pathLetter *root);
void printTrieContents(struct pathLetter *root, int stackTop,char *buffer);
int freeTrieMemory(struct pathLetter *pNode);
int findNextWord(char *pageText, int numBytes, char *wordFound);
int getText(const char* srcAddr, char* buffer, const int bufSize);
int getCount(const char *word, struct pathLetter *root,const int wordLength);

#endif